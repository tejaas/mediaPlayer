angular.module('starter.services', [])

  .factory("audioFactory",function ($firebaseArray) {
    var audioRef = firebase.database().ref().child('audio');
    return $firebaseArray(audioRef);
  })
